import Lexer.Lexer;
import Lexer.Lexem;
import Lexer.Token;
import Optimizer.PolizToTriad;
import Parcer.Parser;
import Parcer.Poliz;
import Stack_Maschine.PolizCalculation;

import java.util.*;

import static java.util.Collections.copy;
import static java.util.Collections.reverseOrder;
import static java.util.Collections.sort;

public class Main {

    public static void main(String[] args) {

        Lexer lexer = new Lexer();
      //  LinkedList<Token> tokens = lexer.lex("c = 1; i = 0; a new List; a.add(5+5+5+5); a.add(10); a.add(5); b new Set; b.add(3); b.add(3); while(i < 5){i = i + 1;} c++; c++;" );
        LinkedList<Token> tokens = lexer.lex("i = 1+1; i = 3; j = 6*i+i;");
        System.out.println("Токены:");
        for (int i = 0; i < tokens.size(); i++)
        {
            System.out.println(tokens.get(i));
        }
        try {
            Parser.parse(tokens);
        }catch ( Exception ex)
        { System.err.println(ex);
            System.exit(1);
        }
        System.out.println("ОПЗ:");
        LinkedList<Token> testPoliz = Poliz.makePoliz(tokens);
        int i = 0;
        for (Token token : testPoliz) {
            System.out.println(i + " " + token);
            i++;
        }

        Set<String> usedEl = new HashSet<String>();
        LinkedList<Token> optPoliz = new LinkedList<Token>();
        LinkedList<Token> queue = new LinkedList<Token>();
        ArrayList<Integer> deleted = new ArrayList<>();
        int begin = 0;
        int flag = 0;
        for (int j = 0; j < testPoliz.size(); j++)
        {
            Token token = testPoliz.get(j);
            if (flag == 0){
                if (token.type == Lexem.OP || token.type == Lexem.ASSIGN_OP || (token.type == Lexem.VAR && !usedEl.contains(token.data)) || token.type == Lexem.NUM)
                {
                    queue.add(token);
                    if (token.type == Lexem.ASSIGN_OP){
                        for (i = 0; i < queue.size(); i++)
                        {
                            optPoliz.add(queue.get(i));
                        }
                        for (i = begin; i < j+1; i++){
                            deleted.add(i);
                        }
                        begin = j+1;
                        queue.clear();
                    }
                }
                else {
                    begin = j+1;
                    for (i = 0; i < queue.size(); i++)
                    {
                        if (queue.get(i).type == Lexem.VAR){
                            usedEl.add(queue.get(i).data);
                        }
                    }
                    queue.clear();
                }
            }
            if (token.type == Lexem.BOOL_OP)
            {
                flag = 1;
            }
            if (token.type == Lexem.GOTO && token.data.equals("!")){
                begin = j+1;
                flag = 0;
            }
        }
        for (i = deleted.size()-1; i >= 0; i--){
            testPoliz.remove((int)deleted.get(i));
        }


        LinkedList<Token> optimizedPoliz = new PolizToTriad(optPoliz).makeTriad();

        for (i = 0; i < optimizedPoliz.size(); i++){
            testPoliz.add(i, optimizedPoliz.get(i));
        }
        System.out.println("ОПЗ:");

        i = 0;
        for (Token token : testPoliz) {
            System.out.println(i + " " + token);
            i++;
        }
        System.out.println("Таблица переменных:");
        PolizCalculation.calculate(testPoliz);

    }
}
