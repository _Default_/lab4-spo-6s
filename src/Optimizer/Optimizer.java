package Optimizer;

import Lexer.Lexem;
import Lexer.Token;

import java.util.HashMap;
import java.util.LinkedList;

public class Optimizer {
    static HashMap<String,  Integer> T =  new HashMap();
    public static LinkedList makeOpt(LinkedList<Triad> triads)
    {
        System.out.println("Оптимизированные триады:");
        for (int i = 0; i < triads.size(); i++){
            Triad triad = triads.get(i);
            if (triad.sign.type == Lexem.OP){
                if (triad.right.type == Lexem.LINK)
                {
                    Triad lastTriad = triads.get(Integer.parseInt(triad.right.data));
                    if (lastTriad.sign.type == Lexem.CONST){
                        triad.right = lastTriad.left;
                    }
                }
                if (triad.left.type == Lexem.LINK)
                {
                    Triad lastTriad = triads.get(Integer.parseInt(triad.left.data));
                    if (lastTriad.sign.type == Lexem.CONST){
                        triad.left = lastTriad.left;
                    }
                }
                if (triad.right.type == Lexem.VAR)
                {
                    triad.right = new Token(Lexem.NUM, Integer.toString(T.get(triad.right.data)));
                    System.out.println(triad.sign + " " + triad.left + " " + triad.right);
                }
                if (triad.left.type == Lexem.VAR)
                {
                    triad.left = new Token(Lexem.NUM, Integer.toString(T.get(triad.left.data)));
                    System.out.println(triad.sign + " " + triad.left + " " + triad.right);
                }
                triad.left.data = op(triad);
                triad.right.data = "0";
                triad.sign = new Token(Lexem.CONST, "");
                triads.set(i, triad);
            }
            if (triad.sign.type == Lexem.ASSIGN_OP)
            {
                if ((triad.right.type == Lexem.LINK))
                {
                    Triad lastTriad = triads.get(Integer.parseInt(triad.right.data));
                    if (lastTriad.sign.type == Lexem.CONST){
                        triad.right = lastTriad.left;
                    }
                }
                if (triad.right.type == Lexem.VAR)
                {
                    triad.right = new Token(Lexem.NUM, Integer.toString(T.get(triad.right.data)));
                }
                T.put(triad.left.data, Integer.parseInt(triad.right.data));
            }
            System.out.println(triad.sign + " " + triad.left + " " + triad.right);
        }

        return new TriadToPoliz(T).makeTriadToPoliz();
    }

    public static String op(Triad triad)
    {

        if (triad.sign.data.equals("+")){
            return Integer.toString(Integer.parseInt(triad.right.data) + Integer.parseInt(triad.left.data));
        }
        if (triad.sign.data.equals("-")){
            return Integer.toString(Integer.parseInt(triad.right.data) - Integer.parseInt(triad.left.data));
        }
        if (triad.sign.data.equals("*")){
            return Integer.toString(Integer.parseInt(triad.right.data) * Integer.parseInt(triad.left.data));
        }
        if (triad.sign.data.equals("/")){
            return Integer.toString(Integer.parseInt(triad.left.data) / Integer.parseInt(triad.right.data));
        }
    return "";
    }
}
