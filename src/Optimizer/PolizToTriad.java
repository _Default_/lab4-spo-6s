package Optimizer;
import Lexer.Lexem;
import Lexer.Token;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import static java.util.Collections.copy;

public class PolizToTriad {
    static LinkedList<Triad> triads = new LinkedList<>();
    static LinkedList<Token> poliz = new LinkedList<>();
   public PolizToTriad(LinkedList<Token> poliz)
    {
        this.poliz = poliz;
    }

    public static LinkedList<Token> makeTriad()
    {
        Stack<Token> stack = new Stack<>();
        for (Token token: poliz){
            if (token.type != Lexem.ASSIGN_OP && token.type != Lexem.OP){
                stack.push(token);
            }
            else
            {
                Token right = stack.pop();
                triads.add(new Triad(token, stack.pop(), right));
                stack.push(new Token(Lexem.LINK, Integer.toString(triads.size()-1)));
            }
        }
        System.out.println("Триады:");
        for (Triad triad: triads){
            System.out.println(triad.sign + " " + triad.left + " " + triad.right);
        }

        return new Optimizer().makeOpt(triads);
    }

}
