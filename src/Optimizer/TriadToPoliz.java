package Optimizer;

import Lexer.Lexem;
import Lexer.Token;

import java.util.HashMap;
import java.util.LinkedList;

public class TriadToPoliz {
    public static HashMap<String, Integer> T = new HashMap<>();
    public static LinkedList<Token> poliz = new LinkedList<>();
    TriadToPoliz(HashMap T){
        this.T = T;
    }
    public static LinkedList makeTriadToPoliz(){
        for (String nameToken: T.keySet()){
            poliz.add(new Token(Lexem.VAR, nameToken));
            poliz.add(new Token(Lexem.NUM, Integer.toString(T.get(nameToken))));
            poliz.add(new Token(Lexem.ASSIGN_OP, "="));
        }

        return poliz;

    }

}
